**Acerca de este proyecto**

Este proyecto es una demostración sencilla a un posible acercamiento a la domótica por parte de PILARES.

*Para más información acerca de lo que son los PILARES visite la página de facebook Pilares CDMX*

---

## Requerimientos

Se necesitan ciertas aplicaciones para poder ejecutar el programa de forma adecuada.

1. Tener instalado **Arduino IDE** en su computadora.
2. Aplicación de **telegram** en su smartphone.
3. Bot de telegram creado. 
4. Python 3.x instalado.
5. Python-telegram-bot instalado.

---

## Funcionamiento

A través de la aplicación de telegram, se enviará un mensaje al bot creado por el usuario y dicho mensaje aparecerá la pantalla LCD del circuito armado y conectado por medio de Arduino UNO, en este caso.

---

## Más información...

Para más información acerca del código utilizado puede visitar la documentación de [python-telegram-bot](https://python-telegram-bot.org/) y la página de la librería de Arduino [LiquidCrystal](https://www.arduino.cc/en/Reference/LiquidCrystal).

Para información adicional de creación de bot en telegram puede vistar *[https://core.telegram.org/bots#6-botfather](https://core.telegram.org/bots#6-botfather)*

EL vídeo donde se muestra la implementación está en la página oficial de Pilares CDMX y lo puede consultar dando click [AQUÍ](https://www.facebook.com/325332928095841/videos/725294288036066)