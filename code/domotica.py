
#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Se definen algunas handler functions y se pasan al Dispatcher y registran en sus lugares respectivos
El bot se ejecuta hasta que se presione Ctrl+C en la línea de comandos.

Uso:
Recibe mensajes y los envía Arduino para que se muestren en una pantalla LCD de 16x2.
Presione Ctrl-C en la línea de comandos para detenet el bot
"""

import logging, serial, time #bibliotecas necesarias para el funcionamiento

from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

# Habilitar registro
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)


# Se definen los Command handlers. Tienen por parámetros update y
# context. Los Error handlers reciben el objeto TelegramError generado en tal caso.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi, ¡estoy listo para la acción!')


def help_command(update, context):
    """Manda mensaje simulando llamar al 911 jaja"""
    update.message.reply_text('Llamando al 911...')
    update.message.reply_text('Espere, por favor')
    update.message.reply_text('...')
    time.sleep(2)
    update.message.reply_text('Lo sentimos, el número que usted marcó no existe. Favor de verificarlo.')

def pilares_command(update, context):
    """Brinda información de qué es PILARES"""
    update.message.reply_text('Puntos de Innovación, Libertad, Arte, Educación y Saberes (PILARES)')
    update.message.reply_text('Puedes checar las convocatorias en https://www.sectei.cdmx.gob.mx/convocatorias-sectei/convocatorias-pilares')


def comunicacion(update, context):
    """Establece la comunicación con Arduino, enviando el mensaje del usuario"""
    arduino= serial.Serial('COM4', 9600)
    time.sleep(2) #espera a que se haga la conexión serial
    update.message.reply_text("Mensaje \""+update.message.text+"\" enviado con éxito")
    arduino.write(update.message.text.encode()) #encode convierte los caracteres UTF-8 a cadenas de 8 bits para ASCII
    arduino.close()

def main():
    """Inicializa el bot."""
    # Creación del Updater. Requiere el token de acceso del bot.
    updater = Updater("TOKEN", use_context=True) #aquí va el token del bot generado por telegram

    # Llama al dispatcher topara el registro de handlers
    dp = updater.dispatcher

    # respuesta del bot a diferentes comandos
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help_command))
    dp.add_handler(CommandHandler("pilares", pilares_command))

    # acción a mensajes sin comandos
    dp.add_handler(MessageHandler(Filters.text & ~Filters.command, comunicacion))

    # Inicializa el bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()
    
    

if __name__ == '__main__':
    main()