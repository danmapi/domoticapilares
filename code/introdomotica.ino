/* 
 *  Domótica introductoria
 *  PILARES | HABILIDADES DIGITALES
 *  Author: DMP.
 */
 
#include <LiquidCrystal.h> // Incluye la Libreria LiquidCrystal

LiquidCrystal lcd(12, 11, 5, 4, 3, 2); // Crea un Objeto LC. Parametros: (rs, enable, d4, d5, d6, d7)

void setup() {   
  Serial.begin(9600);
  lcd.begin(16,2); // Inicializa la interface para la pantalla LCD, determinando sus dimensiones (ancho y alto) del display
  lcd.clear();     //limpia la pantalla
  lcd.setCursor(0,0); //posición del cursor
  lcd.print("Mejor que wasap"); //mensaje
}
void loop() { 
  if (Serial.available()>0){
      String option = Serial.readString(); //recibe el mensaje por comuniación serial con python
      lcd.setCursor(0,1); //posición del cursor para el mensaje recibido
      if(option!="bai"){  //acción específica para las cadena "bai"
        if(option== "bm"){
          option="";      //se verá reflejado que borra el mensaje en la pantalla LCD
        }
        lcd.print(option);  
      }else{
        lcd.clear();
      }         
      
  }
  
 }
